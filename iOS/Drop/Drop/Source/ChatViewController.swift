//  Copyright (c) 2015 Atlassian. All rights reserved.

import UIKit
import DropCore

// MARK: - Properties and Initializers
class ChatViewController: UIViewController {
  @IBOutlet weak var chatInputTextView: UITextView!
  @IBOutlet weak var jsonButton: UIButton!
  
  let typingAttributes = [NSForegroundColorAttributeName: UIColor.blackColor(),
                          NSFontAttributeName: UIFont(name: "Arcon-Regular", size: 22)!]
}

// MARK: - View Controller Lifecycle
extension ChatViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    configureJSONButton(jsonButton)
    configureChatInput(chatInputTextView)
  }
}

// MARK: - Configurators
extension ChatViewController {
  func configureJSONButton(button: UIButton) {
    jsonButton.tintColor = UIColor.actionColor()
  }
  
  func configureChatInput(textView: UITextView) {
    textView.becomeFirstResponder()
    textView.delegate = self
    textView.typingAttributes = typingAttributes
  }
}

// MARK: - Chat Input
extension ChatViewController: UITextViewDelegate {
  func textViewDidChange(textView: UITextView) {
    let detectAndStyleAction =
      DetectAndStyleTokensAction(message: textView.text, factory: HipChatTokenizationFactory())
    detectAndStyleAction.start()
    
    styleChatInput(textView, withStringAttributeTokenMap: detectAndStyleAction.stringAttributeTokenMap)
  }
  
  func styleChatInput(textView: UITextView, withStringAttributeTokenMap attributeTokenMap: [TokenType:TokensWithStringAttributes]) {
    textView.textStorage.beginEditing()
    textView.textStorage.addAttributes(typingAttributes, range: textView.text.absoluteRange)
    for (type, tokensWithAttributes) in attributeTokenMap {
      for (token, attributes) in tokensWithAttributes {
        textView.textStorage.addAttributes(attributes, range: token.range)
      }
    }
    textView.textStorage.endEditing()
    textView.typingAttributes = typingAttributes
  }
}

// MARK: - Navigation
extension ChatViewController {
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let tokenViewController = segue.destinationViewController as? TokenViewController {
      tokenViewController.message = self.chatInputTextView.text
    }
  }
}
