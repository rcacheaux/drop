//  Copyright (c) 2015 Atlassian. All rights reserved.

import UIKit
import DropCore

// MARK: - Properties and Initializers
class TokenViewController: UIViewController {
  @IBOutlet weak var jsonTextView: UITextView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet weak var okButton: UIButton!
  
  var message: String = ""
  var messageToTokenJSONAction: MessageToTokenJSONAction?
}

// MARK: - View Controller Lifecyle
extension TokenViewController {
  override func viewDidLoad() {
    configureOKButton(okButton)
    convertChatMessageToTokenJSON()
  }
}

// MARK: - Configurators
extension TokenViewController {
  func configureOKButton(button: UIButton) {
    button.tintColor = UIColor.actionColor()
    button.backgroundColor = UIColor.actionColor()
    button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
  }
}

// MARK: - Actions (Business Logic)
extension TokenViewController {
  func convertChatMessageToTokenJSON() {
    messageToTokenJSONAction = MessageToTokenJSONAction(message: message, factory: HipChatTokenizationFactory()) {
      json in
      dispatch_async(dispatch_get_main_queue()) {
        self.activityIndicator.stopAnimating()
        self.jsonTextView.text = json
        self.jsonTextView.font = UIFont(name: "SourceCodePro-Regular", size: 14)!
      }
    }
    messageToTokenJSONAction?.start()
  }
}

// MARK: - Navigation
extension TokenViewController {
  @IBAction func ok(sender: AnyObject) {
    dismissViewControllerAnimated(true, completion: nil)
  }
}
