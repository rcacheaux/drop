//  Copyright (c) 2015 Elements. All rights reserved.

import Foundation

public class Action: NSOperation {
  override public func main() {
    if cancelled {
      return
    }
    autoreleasepool {
      self.run()
    }
  }
  
  public func run() {
    preconditionFailure("Action.run() must be overridden.")
  }
}
