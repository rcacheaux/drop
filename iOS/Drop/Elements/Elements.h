//  Copyright (c) 2015 Elements. All rights reserved.

#import <UIKit/UIKit.h>

//! Project version number for Elements.
FOUNDATION_EXPORT double ElementsVersionNumber;

//! Project version string for Elements.
FOUNDATION_EXPORT const unsigned char ElementsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Elements/PublicHeader.h>
