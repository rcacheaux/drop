//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import Elements
import HipChatTokens

public class TokenMapToTokenJSONAction: Action {
  private let tokenMap: [TokenType:Tokens]
  public private(set) var JSON: String = ""
  
  init(tokenMap: [TokenType:Tokens]) {
    self.tokenMap = tokenMap
    super.init()
  }
  
  override public func run() {
    let jsonDictionary = jsonDictionaryFromTokenMap(tokenMap)
    if let jsonString = serializeJSONDictionary(jsonDictionary) {
      JSON = jsonString
    }
  }
  
  func jsonDictionaryFromTokenMap(tokenMap: [TokenType:Tokens]) -> [String : AnyObject] {
    var jsonDictionary: [String : AnyObject] = [:]
    for (tokenType, tokenDictionary) in tokenMap {
      if let hipChatTokenType = HipChatTokenType(rawValue: tokenType) {
        let jsonArray = hipChatTokenType.jsonArrayForTokens(tokenDictionary)
        if jsonArray.count > 0 {
          jsonDictionary[hipChatTokenType.jsonKey] = hipChatTokenType.jsonArrayForTokens(tokenDictionary)
        }
      }
    }
    return jsonDictionary
  }
  
  func serializeJSONDictionary(jsonDictionary: [String : AnyObject]) -> String? {
    if NSJSONSerialization.isValidJSONObject(jsonDictionary) {
      if let jsonData = NSJSONSerialization.dataWithJSONObject(jsonDictionary, options: NSJSONWritingOptions.PrettyPrinted, error: nil) {
        if let string = NSString(data: jsonData, encoding: NSUTF8StringEncoding) {
          return decodeJSONString(string) as String
        }
      }
    }
    return nil
  }
  
  func decodeJSONString(string: NSString) -> NSString {
    let mutableString = NSMutableString(string: string)
    mutableString.replaceOccurrencesOfString("\\/", withString: "/", options: NSStringCompareOptions.allZeros, range: mutableString.absoluteRange)
    mutableString.replaceOccurrencesOfString("\\\"", withString: "\"", options: NSStringCompareOptions.allZeros, range: mutableString.absoluteRange)
    mutableString.replaceOccurrencesOfString("\\\\", withString: "\\", options: NSStringCompareOptions.allZeros, range: mutableString.absoluteRange)
    mutableString.replaceOccurrencesOfString("\\b", withString: "\u{0008}", options: NSStringCompareOptions.allZeros, range: mutableString.absoluteRange)
    mutableString.replaceOccurrencesOfString("\\f", withString: "\u{000C}", options: NSStringCompareOptions.allZeros, range: mutableString.absoluteRange)
    mutableString.replaceOccurrencesOfString("\\n", withString: "\n", options: NSStringCompareOptions.allZeros, range: mutableString.absoluteRange)
    mutableString.replaceOccurrencesOfString("\\r", withString: "\r", options: NSStringCompareOptions.allZeros, range: mutableString.absoluteRange)
    mutableString.replaceOccurrencesOfString("\\t", withString: "\t", options: NSStringCompareOptions.allZeros, range: mutableString.absoluteRange)
    return mutableString.copy() as! NSString
  }
}
