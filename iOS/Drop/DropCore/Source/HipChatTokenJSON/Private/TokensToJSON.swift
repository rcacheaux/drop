//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import HipChatTokens

private enum LinkJSONKey: String {
  case url = "url"
  case title = "title"
}

public extension HipChatTokenType {
  public var jsonKey: String {
    switch self {
    case .Emoticon:
      return "emoticons"
    case .Hyperlink:
      return "links"
    case .Mention:
      return "mentions"
    }
  }
  
  public func jsonArrayForTokens(tokens: Tokens) -> [AnyObject] {
    switch self {
    case .Emoticon:
      var jsonElements: [String] = []
      for (token, metadata) in tokens {
        if let semanticDescription = metadata[tokenSemanticDescriptionKey] as? String {
          jsonElements.append(semanticDescription)
        } else {
          jsonElements.append(token.string)
        }
      }
      return jsonElements
    case .Hyperlink:
      var jsonElements: [[String:AnyObject]] = []
      for (token, metadata) in tokens {
        var JSONDictionary: [String:AnyObject] = [:]
        JSONDictionary[LinkJSONKey.url.rawValue] = token.string
        if let title = metadata[tokenSemanticDescriptionKey] as? String {
          JSONDictionary[LinkJSONKey.title.rawValue] = title
        }
        jsonElements.append(JSONDictionary)
      }
      return jsonElements
    case .Mention:
      var jsonElements: [String] = []
      for (token, metadata) in tokens {
        if let semanticDescription = metadata[tokenSemanticDescriptionKey] as? String {
          jsonElements.append(semanticDescription)
        } else {
          jsonElements.append(token.string)
        }
      }
      return jsonElements
    }
  }
}
