//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import Elements
import HipChatTokens

public class MessageToTokenJSONAction: AsyncAction {
  public private(set) var JSON: String = ""
  
  private let message: String
  private let factory: TokenizationEngineFactory
  private let onComplete: ( (json: String)->() )?
  private let operationQueue = NSOperationQueue()
  
  public init(message: String, factory: TokenizationEngineFactory, onComplete:(json: String)->()) {
    self.message = message
    self.factory = factory
    self.onComplete = onComplete
    super.init()
  }
  
  override public func actionCompleted() {
    onComplete?(json: JSON)
  }
  
  override public func run() {
    let matchEnrichAction = DetectAndEnrichTokensAction(message: message, factory: factory) {
      result in
      let tokenToJSONAction = TokenMapToTokenJSONAction(tokenMap: result)
      tokenToJSONAction.completionBlock = {
        [weak self, weak tokenToJSONAction] in
        if let strongSelf = self, let strongAction = tokenToJSONAction {
          strongSelf.JSON = strongAction.JSON
          strongSelf.finishExecutingOperation()
        }
      }
      self.operationQueue.addOperation(tokenToJSONAction)
    }
    operationQueue.addOperation(matchEnrichAction)
  }
}
