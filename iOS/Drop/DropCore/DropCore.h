//  Copyright (c) 2015 Atlassian. All rights reserved.

#import <UIKit/UIKit.h>

//! Project version number for DropCore.
FOUNDATION_EXPORT double DropCoreVersionNumber;

//! Project version string for DropCore.
FOUNDATION_EXPORT const unsigned char DropCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DropCore/PublicHeader.h>

@import HipChatTokens;
