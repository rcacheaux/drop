//  Copyright (c) 2015 Atlassian. All rights reserved.

import XCTest
import HipChatTokens

/*
Emoticon spec:
For this exercise, you only need to consider 'custom' emoticons which are ASCII strings, no longer than 15 characters, contained in parenthesis. You can assume that anything matching this format is an emoticon.
*/

// MARK: - Single Emoticon Tests
class EmoticonTokenizerTests: XCTestCase {
  // (megusta)
  func testSimpleSingleEmoticon() {
    let emoticon = Emoticon(emoticon: "(megusta)", location: 0)
    let chatMessage = "\(emoticon)"
    
    AssertEmoticonDetection(message: chatMessage, expectEmoticons: [emoticon])
  }
  
  // (plus1)
  func testEmoticonWithNumber() {
    let emoticon = Emoticon(emoticon: "(plus1)", location: 0)
    let chatMessage = "\(emoticon)"
    
    AssertEmoticonDetection(message: chatMessage, expectEmoticons: [emoticon])
  }
  
  // number (50210) emoticon
  func testNumberEmoticon() {
    let emoticon = Emoticon(emoticon: "(50210)", location: 7)
    let chatMessage = "number \(emoticon) emoticon"
    
    AssertEmoticonDetection(message: chatMessage, expectEmoticons: [emoticon])
  }
  
  // ascii symbol (!+!^*_-) emoticon
  func testNonWordAsciiEmoticon() {
    let emoticon = Emoticon(emoticon: "(!+!^*_-)", location: 13)
    let chatMessage = "ascii symbol \(emoticon) emoticon"
    
    AssertEmoticonDetection(message: chatMessage, expectEmoticons: [emoticon])
  }
  
  // (one up) for John!
  func testWhitespaceEmoticon() {
    let emoticon = Emoticon(emoticon: "(one up)", location: 0)
    let chatMessage = "\(emoticon) for John!"
    
    AssertEmoticonDetection(message: chatMessage, expectEmoticons: [emoticon])
  }
  
  // ((one up)) for John!
  func testDoubleWrappedParenthesis() {
    let emoticon = Emoticon(emoticon: "(one up)", location: 1)
    let chatMessage = "(\(emoticon)) for John!"
    
    AssertEmoticonDetection(message: chatMessage, expectEmoticons: [emoticon])
  }
  
  //  (hey(there))!
  func testEmbeddedParenthesis() {
    let emoticon = Emoticon(emoticon: "(there)", location: 5)
    let chatMessage = " (hey\(emoticon))!"
    
    AssertEmoticonDetection(message: chatMessage, expectEmoticons: [emoticon])
  }
  
  // this: (aNemoticonbarel) is an emoticon
  func testExact15CharacterEmoticon() {
    let emoticon = Emoticon(emoticon: "(anemoticonbarel)", location: 6)
    let chatMessage = "this: \(emoticon) is an emoticon"
    
    AssertEmoticonDetection(message: chatMessage, expectEmoticons: [emoticon])
  }
  
  // this:(yes)! is an emoticon
  func testPunctuationSurroundingEmoticon() {
    let emoticon = Emoticon(emoticon: "(yes)", location: 5)
    let chatMessage = "this:\(emoticon)! is an emoticon"
    
    AssertEmoticonDetection(message: chatMessage, expectEmoticons: [emoticon])
  }
}

// MARK: - Multiple Emoticon Tests
extension EmoticonTokenizerTests {
  // yo, (doh) (lol)
  func testMultipleParentsWithin15Characters() {
    let emoticons = [Emoticon(emoticon: "(doh)", location: 4),
                     Emoticon(emoticon: "(lol)", location: 10)]
    let chatMessage = "yo, \(emoticons[0]) \(emoticons[1])"
    
    AssertEmoticonDetection(message: chatMessage, expectEmoticons: emoticons)
  }
  
  // yo, (doh)(lol)
  func testMultipleParentsWithin15CharactersNoSpace() {
    let emoticons = [Emoticon(emoticon: "(doh)", location: 4),
                     Emoticon(emoticon: "(lol)", location: 9)]
    let chatMessage = "yo, \(emoticons[0])\(emoticons[1])"
    
    AssertEmoticonDetection(message: chatMessage, expectEmoticons: emoticons)
  }
  
  func testSameEmoticonBackToBack() {
    let emoticons = [Emoticon(emoticon: "(cake)", location: 5),
                     Emoticon(emoticon: "(cake)", location: 11)]
    let chatMessage = "yum, \(emoticons[0])\(emoticons[1])"
    
    AssertEmoticonDetection(message: chatMessage, expectEmoticons: emoticons)
  }
  
  // hey(allthethings)(boom)(chompy)(hipchat)(tableflip)(yougotitdude)(foreveralone)
  func testBackToBack() {
    let emoticons = [Emoticon(emoticon: "(allthethings)", location: 3),
                     Emoticon(emoticon: "(boom)", location: 17),
                     Emoticon(emoticon: "(chompy)", location: 23),
                     Emoticon(emoticon: "(hipchat)", location: 31),
                     Emoticon(emoticon: "(tableflip)", location: 40),
                     Emoticon(emoticon: "(yougotitdude)", location: 51),
                     Emoticon(emoticon: "(foreveralone)", location: 65)]
    let allEmoticons = emoticons.reduce("") { $0.description + $1.description}
    let chatMessage = "hey\(allEmoticons) aw yea"
    
    AssertEmoticonDetection(message: chatMessage, expectEmoticons: emoticons)
  }
  
  // only two () (é) (allthethings) of these (thisisareallylongstring) (excellent) are emoticons
  func testPositiveNegativeMatch() {
    let emoticons = [Emoticon(emoticon: "(allthethings)", location: 16),
                     Emoticon(emoticon: "(excellent)", location: 66)]
    let chatMessage = "only two () (é) \(emoticons[0]) of these (thisisareallylongstring) \(emoticons[1]) are emoticons"
    
    AssertEmoticonDetection(message: chatMessage, expectEmoticons: emoticons)
  }
}

// MARK: - Negative Tests
extension EmoticonTokenizerTests {
  func testEmptyString() {
    AssertNoEmoticonsDetected("")
  }
  
  func testSimpleNoEmoticon() {
    AssertNoEmoticonsDetected("there's definitely no emoticons here awyea")
  }
  
  func testNonAscii() {
    AssertNoEmoticonsDetected("no (émøtîcons) here ay?")
  }
  
  func testDualByteUTF8() {
    AssertNoEmoticonsDetected("this is (नहीं) an emoticon")
  }
  
  func testLongerThan15Characters() {
    AssertNoEmoticonsDetected("hey there, (noemoticonherepl)")
  }
  
  func testEmoji() {
    AssertNoEmoticonsDetected("(🐶) cute but not an emoticon")
  }
  
  func testEmptyParenths() {
    AssertNoEmoticonsDetected("none here ()")
  }
  
  func testNoEndingParenthesis() {
    AssertNoEmoticonsDetected("no (emo ticon here")
  }
  
  func testNoEndingParenthesisEndOfLine() {
    AssertNoEmoticonsDetected("not an (emoticon")
  }
  
  func testOnlyClosingParenthesis() {
    AssertNoEmoticonsDetected("looks like an emoticon) but it's not")
  }
}

// MARK: - Performance Tests
extension EmoticonTokenizerTests {
  func testPerformanceSimpleMessage() {
    self.measureBlock() {
      let message = "oh no! (facepalm)"
      let tokens = EmoticonTokenDetector().tokensInMessage(message)
    }
  }
  
  func testPerformanceComplexMessage() {
    self.measureBlock() {
      let message = "(gates) ((goodnews)(cookie) (disappear) (dosequis) (dealwithit) (excellent) (उपवास) () ()))) (🍻)"
      let tokens = EmoticonTokenDetector().tokensInMessage(message)
    }
  }
}

// MARK: - Test Syntax Sugar

private func AssertEmoticonDetection(#message: String, #expectEmoticons: [Emoticon]) {
  let tokens = EmoticonTokenDetector().tokensInMessage(message)
  for emoticon in expectEmoticons {
    XCTAssert(tokens[emoticon.toToken()] != nil, "")
  }
  XCTAssert(tokens.count == expectEmoticons.count, "")
}

private func AssertNoEmoticonsDetected(message: String) {
  XCTAssert(EmoticonTokenDetector().tokensInMessage(message).count == 0)
}

extension Token {
  private init(emoticon: String, range: NSRange) {
    self.init(type: tokenType, string: emoticon, range: range)
  }
}

extension EmoticonTokenDetector {
  private convenience init() {
    self.init(tokenType: tokenType)
  }
}

private let tokenType = HipChatTokenType.Emoticon.rawValue

struct Emoticon: Printable, Tokenizable {
  static let type = tokenType
  
  let emoticon: String
  let location: Int
  var description: String { return emoticon }
  
  func toToken() -> Token {
    return Token(emoticon: emoticon, range: NSRange(location: location, length: emoticon.absoluteRange.length))
  }
}
