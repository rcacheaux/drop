//  Copyright (c) 2015 Atlassian. All rights reserved.

import XCTest
import HipChatTokens

class HTMLTitleExtractionTests: XCTestCase {
  func testSimpleHTMLTitle() {
    let title = "Atlassian"
    let html = "<html><head><title>\(title)</title></head><body></body></html>"
    AssertTitle(title, inHTML: html)
  }
  
  func testTab() {
    let title = "\tAtlassian\t"
    let html = "<html><head><title>\(title)</title></head><body></body></html>"
    AssertTitle(title, inHTML: html)
  }
  
  func testNewline() {
    let title = "\rAtlassian \rRocks"
    let html = "<html><head><title>\(title)</title></head><body></body></html>"
    AssertTitle(title, inHTML: html)
  }
  
  func testHTMLEncodedCharactersDash() {
    let title = "Atlassian – Rocks"
    let html = "<html><head><title>Atlassian &#8211; Rocks</title></head><body></body></html>"
    AssertTitle(title, inHTML: html)
  }
  
  func testHTMLEncodedCharactersCopyright() {
    let title = "Atlassian © Rocks"
    let html = "<html><head><title>Atlassian &copy; Rocks</title></head><body></body></html>"
    AssertTitle(title, inHTML: html)
  }
  
  func testSpaces() {
    let title = "Atlassian"
    let html = "<html><head><title    >\(title)</title   ></head><body></body></html>"
    AssertTitle(title, inHTML: html)
  }
  
  func testAttribute() {
    let title = "Atlassian"
    let html = "<html><head><title  lang=\"us\">\(title)</title   ></head><body></body></html>"
    AssertTitle(title, inHTML: html)
  }
  
  func testAttributeWithClosingAngleBracket() {
    let title = "Atlassian ATX"
    let html = "<html><head><title  lang=\"us>\">\(title)</title   ></head><body></body></html>"
    AssertTitle(title, inHTML: html)
  }
}

extension HTMLTitleExtractionTests {
  func AssertTitle(title: String, inHTML html: String) {
    let expectation = self.expectationWithDescription("")
    let remoteAPI = FakeURLRemoteAPI(responseHTMLOrNil: html, errorOrNil: nil)
    let action = ExtractHTMLTitleFromURLAction(url: NSURL(), remoteAPI: remoteAPI) {
      result in
      XCTAssert(result == title, "")
      expectation.fulfill()
    }
    let queue = NSOperationQueue()
    queue.addOperation(action)
    self.waitForExpectationsWithTimeout(1, handler: nil)
  }
}

// Fake implementation used for unit testing.
class FakeURLRemoteAPI: RemoteAPI {
  let responseHTMLOrNil: String?
  let errorOrNil: NSError?
  
  init(responseHTMLOrNil: String?, errorOrNil: NSError?) {
    self.responseHTMLOrNil = responseHTMLOrNil
    self.errorOrNil = errorOrNil
  }
  
  func getHTMLFromURL(url: NSURL, completionQueue: NSOperationQueue, onComplete: (String?, NSError?)->()) {
    let operation = NSBlockOperation {
      onComplete(self.responseHTMLOrNil, self.errorOrNil)
    }
    completionQueue.addOperation(operation)
  }
}
