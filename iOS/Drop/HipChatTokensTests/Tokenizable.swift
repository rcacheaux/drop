//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import HipChatTokens

protocol Tokenizable {
  func toToken() -> Token
}

