//  Copyright (c) 2015 Atlassian. All rights reserved.

import XCTest
import HipChatTokens

// MARK: - Single Hyperlink Tests
class HyperlinkTokenizerTests: XCTestCase {
  // These tests are not as comprehensive as the ones for mentions and emoticons
  //  since the hyperlink token detector depends on Apple's NSDataDetector instead of
  //  a custom regex pattern.
  
  func testDataDetectorBackedTokenizer() {
    // This test won't compile if HyperlinkTokenDetector 
    //   does not inherit from DataDetectorTokenDetector
    let detector: DataDetectorTokenDetector = HyperlinkTokenDetector()
    detector.tokensInMessage("hey there http://www.atlassian.com")
  }
  
  // http://www.atlassian.com is here
  func testSimpleHyperlink() {
    let hyperlink = Hyperlink(hyperlink: "http://www.atlassian.com", location: 0)
    let chatMessage = "\(hyperlink) is here"
    
    AssertHyperlinkDetection(message: chatMessage, expectHyperlinks: [hyperlink])
  }

  // http://www.atlassian.com
  func testMessageIsOnlyAHyperlink() {
    let hyperlink = Hyperlink(hyperlink: "http://www.atlassian.com", location: 0)
    let chatMessage = "\(hyperlink)"
    
    AssertHyperlinkDetection(message: chatMessage, expectHyperlinks: [hyperlink])
  }
  
  // security first https://www.atlassian.com
  func testHTTPS() {
    let hyperlink = Hyperlink(hyperlink: "https://www.atlassian.com", location: 15)
    let chatMessage = "security first \(hyperlink)"
    
    AssertHyperlinkDetection(message: chatMessage, expectHyperlinks: [hyperlink])
  }
  
  // no hypertext! www.atlassian.com
  func testNoProtocol() {
    let hyperlink = Hyperlink(hyperlink: "www.atlassian.com", location: 14)
    let chatMessage = "no hypertext! \(hyperlink)"
    
    AssertHyperlinkDetection(message: chatMessage, expectHyperlinks: [hyperlink])
  }
  
  // hockeyapp.net
  func testNetDomain() {
    let hyperlink = Hyperlink(hyperlink: "hockeyapp.net", location: 0)
    let chatMessage = "\(hyperlink)"
    
    AssertHyperlinkDetection(message: chatMessage, expectHyperlinks: [hyperlink])
  }
  
  func testCountryDomain() {
    let hyperlink = Hyperlink(hyperlink: "http://www.cityoflondon.gov.uk/Pages/default.aspx", location: 0)
    let chatMessage = "\(hyperlink)"
    
    AssertHyperlinkDetection(message: chatMessage, expectHyperlinks: [hyperlink])
  }
  
  // atlassian.com
  func testNoProtocolNoWWW() {
    let hyperlink = Hyperlink(hyperlink: "atlassian.com", location: 0)
    let chatMessage = "\(hyperlink)"
    
    AssertHyperlinkDetection(message: chatMessage, expectHyperlinks: [hyperlink])
  }
  
  // HipChat! https://www.atlassian.com/software/hipchat
  func testMuliplePathURL() {
    let hyperlink = Hyperlink(hyperlink: "https://www.atlassian.com/software/hipchat", location: 9)
    let chatMessage = "HipChat! \(hyperlink)"
    
    AssertHyperlinkDetection(message: chatMessage, expectHyperlinks: [hyperlink])
  }
}

// MARK: - Multiple Hyperlink Tests
extension EmoticonTokenizerTests {
  func testSimpleTwoHyperlinkMessage() {
    let hyperlinks = [Hyperlink(hyperlink: "http://www.atlassian.com", location: 0),
                      Hyperlink(hyperlink: "http://www.nike.com", location: 25)]
    let chatMessage = "\(hyperlinks[0]) \(hyperlinks[1])"
    
    AssertHyperlinkDetection(message: chatMessage, expectHyperlinks: hyperlinks)
  }
}


// MARK: - Test Syntax Sugar

private func AssertHyperlinkDetection(#message: String, #expectHyperlinks: [Hyperlink]) {
  let tokens = HyperlinkTokenDetector().tokensInMessage(message)
  for hyperlink in expectHyperlinks {
    XCTAssert(tokens[hyperlink.toToken()] != nil, "")
  }
  XCTAssert(tokens.count == expectHyperlinks.count, "")
}

private func AssertNoHyperlinksDetected(message: String) {
  XCTAssert(HyperlinkTokenDetector().tokensInMessage(message).count == 0)
}

extension HyperlinkTokenDetector {
  private convenience init() {
    self.init(tokenType: tokenType)
  }
}

extension Token {
  private init(hyperlink: String, range: NSRange) {
    self.init(type: tokenType, string: hyperlink, range: range)
  }
}

private let tokenType = HipChatTokenType.Hyperlink.rawValue

struct Hyperlink: Printable, Tokenizable {
  static let type = tokenType
  
  let hyperlink: String
  let location: Int
  var description: String { return hyperlink }
  
  func toToken() -> Token {
    return Token(hyperlink: hyperlink, range: NSRange(location: location, length: hyperlink.absoluteRange.length))
  }
}
