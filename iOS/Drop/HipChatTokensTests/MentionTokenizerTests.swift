//  Copyright (c) 2015 Atlassian. All rights reserved.

import XCTest
import HipChatTokens

/*
Mention Spec:
A way to mention a user. Always starts with an '@' and ends when hitting a non-word character.
*/

// MARK: - Single Mention Tests
class MentionTokenizerTests: XCTestCase {
  // hey @t
  func testSingleCharacter() {
    let mention = Mention(mention: "@t", location: 4)
    let chatMessage = "hey \(mention)"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // hey @rene
  func testSimpleMention() {
    let mention = Mention(mention: "@rene", location: 4)
    let chatMessage = "hey \(mention)"

    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // hey @rene!
  func testSimpleMentionPunctuation() {
    let mention = Mention(mention: "@rene", location: 4)
    let chatMessage = "hey \(mention)!"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // hey @rene!!!
  func testSimpleMentionMultiplePunctuation() {
    let mention = Mention(mention: "@rene", location: 4)
    let chatMessage = "hey \(mention)!!!"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // hey, @1up, Mario calls...
  func testNumberInMention() {
    let mention = Mention(mention: "@1up", location: 5)
    let chatMessage = "hey, \(mention), Mario calls..."
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // hey, @50210, your lambo is outside!
  func testNumberMention() {
    let mention = Mention(mention: "@50210", location: 5)
    let chatMessage = "hey, \(mention), your lambo is outside!"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // hey@rene
  func testNoWhitespace() {
    let mention = Mention(mention: "@rene", location: 3)
    let chatMessage = "hey\(mention)"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // hey@rene what's up
  func testSandwiched() {
    let mention = Mention(mention: "@rene", location: 3)
    let chatMessage = "hey\(mention) what's up"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // "hey@@rene what's up
  func testDoubleAt() {
    let mention = Mention(mention: "@rene", location: 4)
    let chatMessage = "hey@\(mention) what's up"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // hey@@@@@rene what's up
  func testMultipleAt() {
    let mention = Mention(mention: "@rene", location: 7)
    let chatMessage = "hey@@@@\(mention) what's up"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // hey@@@@@rene@ what's up
  func testAtSandwich() {
    let mention = Mention(mention: "@rene", location: 7)
    let chatMessage = "hey@@@@\(mention)@ what's up"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // hey@rené
  func testUTF8() {
    let mention = Mention(mention: "@rené", location: 3)
    let chatMessage = "hey\(mention)"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // hey @@私はガラスを食べら
  func testUTF8MultiByte() {
    let mention = Mention(mention: "@私はガラスを食べら", location: 3)
    let chatMessage = "hey\(mention)"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // を食べられ私はガラスを食べら@私はガラスを食べら
  func testFullUTF8MultiByteMessage() {
    let mention = Mention(mention: "@私はガラスを食べら", location: 14)
    let chatMessage = "を食べられ私はガラスを食べら\(mention)"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // hey @😇
  func testSingleCharacterEmoji() {
    let mention = Mention(mention: "@😇", location: 4)
    let chatMessage = "hey \(mention)"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // hey @🍔👍👏
  func testMultipleCharacterEmoji() {
    let mention = Mention(mention: "@🍔👍👏", location: 4)
    let chatMessage = "hey \(mention)"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
  
  // hey @i❤️coffee
  func testMixedAsciiEmoji() {
    let mention = Mention(mention: "@i❤️coffee", location: 4)
    let chatMessage = "hey \(mention)"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
}

// MARK: - Multiple Mention Tests
extension MentionTokenizerTests {
  // hey @rene @here
  func testMultipleMentions() {
    let mentions = [Mention(mention: "@rene", location: 4),
                    Mention(mention: "@here", location: 10)]
    let chatMessage = "hey \(mentions[0]) \(mentions[1])"
    
    AssertMentionDetection(message: chatMessage, expectMentions: mentions)
  }
  
  // hey @rene@here
  func testMultipleMentionsWithoutWhitespace() {
    let mentions = [Mention(mention: "@rene", location: 3),
                    Mention(mention: "@here", location: 8)]
    let chatMessage = "hey\(mentions[0])\(mentions[1]) what's up"
    
    AssertMentionDetection(message: chatMessage, expectMentions: mentions)
  }
  
  //hey@rené@was@ที่นี่@t@three@o@🕐 aw yea
  func testBackToBack() {
    let mentions = [Mention(mention: "@rené", location: 3),
                    Mention(mention: "@was", location: 8),
                    Mention(mention: "@ที่นี่", location: 12),
                    Mention(mention: "@t", location: 19),
                    Mention(mention: "@three", location: 21),
                    Mention(mention: "@o", location: 27),
                    Mention(mention: "@🕐", location: 29)]
    let allMentions = mentions.reduce("") { $0.description + $1.description}
    let chatMessage = "hey\(allMentions) aw yea"
    
    AssertMentionDetection(message: chatMessage, expectMentions: mentions)
  }
  
  // hey @renehttp://www.atlassian.com @herehttps://www.apple.com
  func testNextToURL() {
    let mentions = [Mention(mention: "@rene", location: 4),
                    Mention(mention: "@here", location: 34)]
    let chatMessage = "hey \(mentions[0])http://www.atlassian.com \(mentions[1])https://www.apple.com"
    
    AssertMentionDetection(message: chatMessage, expectMentions: mentions)
  }
}

// MARK: - Negative Tests
extension MentionTokenizerTests {
  func testEmptyString() {
    AssertNoMentionsDetected("")
  }
  
  func testSimpleNoMention() {
    AssertNoMentionsDetected("Hi friend, no at mentions here, right?")
  }
  
  func testIsolatedAt() {
    AssertNoMentionsDetected("Hi friend, no @ mentions here, right?")
  }
  
  func testIsolatedMultipleAts() {
    AssertNoMentionsDetected("Hi friend, no @@ mentions here, right?")
  }
  
  func testMathCharacter() {
    AssertNoMentionsDetected("Hi friend, no @+ mentions here, right?")
  }
  
  func testMultipleMathCharacters() {
    AssertNoMentionsDetected("Hi friend, no @+--% mentions here, right?")
  }
  
  func testMixedWithMathCharacter() {
    let mention = Mention(mention: "@he", location: 4)
    let chatMessage = "hey \(mention)^e"
    
    AssertMentionDetection(message: chatMessage, expectMentions: [mention])
  }
}

// MARK: - Performance Tests
extension MentionTokenizerTests {
  func testPerformanceSimpleMessage() {
    self.measureBlock() {
      let message = "hey@rene@here what's up"
      let tokens = MentionTokenDetector().tokensInMessage(message)
    }
  }
  
  func testPerformanceComplexMessage() {
    self.measureBlock() {
      let message = "hey@rené@was@ที่นี่@t@three@o@🕐 aw yea @yes @🎶music @☔️مطر @ صبي this is some message @here"
      let tokens = MentionTokenDetector().tokensInMessage(message)
    }
  }
}


// MARK: - Test Syntax Sugar

private func AssertMentionDetection(#message: String, #expectMentions: [Mention]) {
  let tokens = MentionTokenDetector().tokensInMessage(message)
  for mention in expectMentions {
    XCTAssert(tokens[mention.toToken()] != nil, "")
  }
  XCTAssert(tokens.count == expectMentions.count, "")
}

private func AssertNoMentionsDetected(message: String) {
  XCTAssert(MentionTokenDetector().tokensInMessage(message).count == 0)
}

extension MentionTokenDetector {
  private convenience init() {
    self.init(tokenType: tokenType)
  }
}

extension Token {
  private init(mention: String, range: NSRange) {
    self.init(type: tokenType, string: mention, range: range)
  }
}

private let tokenType = HipChatTokenType.Mention.rawValue

struct Mention: Printable, Tokenizable {
  static let type = tokenType
  
  let mention: String
  let location: Int
  var description: String { return mention }
  
  func toToken() -> Token {
    return Token(mention: mention, range: NSRange(location: location, length: mention.absoluteRange.length))
  }
}
