//  Copyright (c) 2015 Atlassian. All rights reserved.

import XCTest
import HipChatTokens

class MultipleTokenizerTests: XCTestCase {
  // And I-I said, I don't care if they lay me off, either. Because I told, I told @Bill that if they move my (desk) one more time, then, then I'm, I'm quitting. I'm going to quit. And, and I told @Don too, because they've moved my (desk) four times already this year, and I used to be over by the window, and I could see the squirrels and they were married, but then they switched from the http://www.swingline.com stapler to the bostitch.com stapler, but I kept my www.swingline.com stapler because it didn't bind up as much and I kept the staples for the www.swingline.com stapler....And, oh, no, it's not (okay) because if they make me, if they, If they take my stapler then I'll, I'll have to, I'll set the building on (fire)
  func testOfficeSpace() {
    let mentions = [Mention(mention: "@Bill", location: 78),
                    Mention(mention: "@Don", location: 192)]
    let emoticons = [Emoticon(emoticon: "(desk)", location: 105),
                     Emoticon(emoticon: "(desk)", location: 227),
                     Emoticon(emoticon: "(okay)", location: 604),
                     Emoticon(emoticon: "(fire)", location: 719)]
    let hyperlinks = [Hyperlink(hyperlink: "http://www.swingline.com", location: 386),
                      Hyperlink(hyperlink: "bostitch.com", location: 426),
                      Hyperlink(hyperlink: "www.swingline.com", location: 462),
                      Hyperlink(hyperlink: "www.swingline.com", location: 553)]
    let chatMessage = "And I-I said, I don't care if they lay me off, either. Because I told, I told \(mentions[0]) that if they move my \(emoticons[0]) one more time, then, then I'm, I'm quitting. I'm going to quit. And, and I told \(mentions[1]) too, because they've moved my \(emoticons[1]) four times already this year, and I used to be over by the window, and I could see the squirrels and they were married, but then they switched from the \(hyperlinks[0]) stapler to the \(hyperlinks[1]) stapler, but I kept my \(hyperlinks[2]) stapler because it didn't bind up as much and I kept the staples for the \(hyperlinks[3]) stapler....And, oh, no, it's not \(emoticons[2]) because if they make me, if they, If they take my stapler then I'll, I'll have to, I'll set the building on \(emoticons[3])"
    
    AssertTokenDetection(message: chatMessage, expectMentions: mentions, expectEmoticons: emoticons, expectHyperlinks: hyperlinks)
  }
  
  // Hello? Hello? @Anybody (home)? Hey! Think, @McFly. Think!
  func testBackToTheFuture() {
    let mentions = [Mention(mention: "@Anybody", location: 14),
                    Mention(mention: "@McFly", location: 42)]
    let emoticons = [Emoticon(emoticon: "(home)", location: 22)]
    let hyperlinks = [Hyperlink]()
    let chatMessage = "Hello? Hello? \(mentions[0])\(emoticons[0])? Hey! Think, \(mentions[1]). Think!"
    
    AssertTokenDetection(message: chatMessage, expectMentions: mentions, expectEmoticons: emoticons, expectHyperlinks: hyperlinks)
  }
  
  // @you guys might not know this, but I consider @myself a bit of a loner. I tend to think of @myself as a one-man wolf pack. But when my sister brought @Doug (home), I knew he was one of my own. And my wolf pack… it grew by one. So there… there were (two) of us in the wolf pack… I was alone first in the pack, and then @Doug joined in later. And six months ago, when @Doug introduced me to you guys, I thought, ‘Wait a second, could it be?’ And now I know for sure, I just added two more guys to my wolf pack. Four of us wolves, running around the desert together, in http://www.lasvegas.com. So (tonight), I make a (toast)!
  func testHangover() {
    let mentions = [Mention(mention: "@you", location: 0),
                    Mention(mention: "@myself", location: 46),
                    Mention(mention: "@myself", location: 91),
                    Mention(mention: "@Doug", location: 150),
                    Mention(mention: "@Doug", location: 318),
                    Mention(mention: "@Doug", location: 366)]
    let emoticons = [Emoticon(emoticon: "(home)", location: 156),
                     Emoticon(emoticon: "(two)", location: 248),
                     Emoticon(emoticon: "(tonight)", location: 595),
                     Emoticon(emoticon: "(toast)", location: 615)]
    let hyperlinks = [Hyperlink(hyperlink: "http://www.lasvegas.com", location: 567)]
    let chatMessage = "\(mentions[0]) guys might not know this, but I consider \(mentions[1]) a bit of a loner. I tend to think of \(mentions[2]) as a one-man wolf pack. But when my sister brought \(mentions[3]) \(emoticons[0]), I knew he was one of my own. And my wolf pack… it grew by one. So there… there were \(emoticons[1]) of us in the wolf pack… I was alone first in the pack, and then \(mentions[4]) joined in later. And six months ago, when \(mentions[5]) introduced me to you guys, I thought, ‘Wait a second, could it be?’ And now I know for sure, I just added two more guys to my wolf pack. Four of us wolves, running around the desert together, in \(hyperlinks[0]). So \(emoticons[2]), I make a \(emoticons[3])!"
    
    AssertTokenDetection(message: chatMessage, expectMentions: mentions, expectEmoticons: emoticons, expectHyperlinks: hyperlinks)
  }
  
  // These guys are supposed to be doing that, @lars http://plumlife.com
  func testSimpleMessage() {
    let mentions = [Mention(mention: "@lars", location: 42)]
    let emoticons = [Emoticon]()
    let hyperlinks = [Hyperlink(hyperlink: "http://plumlife.com", location: 48)]
    let chatMessage = "These guys are supposed to be doing that, \(mentions[0]) \(hyperlinks[0])"
    
    AssertTokenDetection(message: chatMessage, expectMentions: mentions, expectEmoticons: emoticons, expectHyperlinks: hyperlinks)
  }
}

// MARK: - Performance Tests
extension MultipleTokenizerTests {
  func testPerformanceComplexMessage() {
    self.measureBlock() {
      let message = "And I-I said, I don't care if they lay me off, either. Because I told, I told @Bill that if they move my (desk) one more time, then, then I'm, I'm quitting. I'm going to quit. And, and I told @Don too, because they've moved my (desk) four times already this year, and I used to be over by the window, and I could see the squirrels and they were married, but then they switched from the http://www.swingline.com stapler to the bostitch.com stapler, but I kept my www.swingline.com stapler because it didn't bind up as much and I kept the staples for the www.swingline.com stapler....And, oh, no, it's not (okay) because if they make me, if they, If they take my stapler then I'll, I'll have to, I'll set the building on (fire)"
      let queue = NSOperationQueue()
      let action = DetectTokensAction(message: message) { result in }
      queue.addOperations([action], waitUntilFinished: true)
    }
  }
}


// MARK: - Test Syntax Sugar

extension MultipleTokenizerTests {
  func AssertTokenDetection(#message: String, expectMentions: [Mention], expectEmoticons: [Emoticon], expectHyperlinks: [Hyperlink]) {
    let expectation = self.expectationWithDescription("")
    
    let action = DetectTokensAction(message: message) { result in
      let expectTokenMap = tokenMapWithMentions(expectMentions,
        emoticons: expectEmoticons, hyperlinks: expectHyperlinks)
      for (type, tokens) in result {
        let expectTokens = expectTokenMap[type]!
        XCTAssert(tokens.count == expectTokens.count, "")
        for (expectToken, metadata) in expectTokens {
          XCTAssert(tokens[expectToken] != nil, "")
        }
      }
      expectation.fulfill()
    }
    
    action.start()
    self.waitForExpectationsWithTimeout(1, handler: expectationHandler)
  }
}

func tokensFromTokenizables<T: Tokenizable>(tokenizables: [T]) -> Tokens {
  var tokens = Tokens()
  for tokenizable in tokenizables {
    tokens[tokenizable.toToken()] = TokenMetadata()
  }
  return tokens
}

func tokenMapWithMentions(mentions: [Mention], #emoticons: [Emoticon], #hyperlinks: [Hyperlink]) -> [TokenType:Tokens] {
  var tokenMap = [TokenType:Tokens]()
  tokenMap[Mention.type] = tokensFromTokenizables(mentions)
  tokenMap[Emoticon.type] = tokensFromTokenizables(emoticons)
  tokenMap[Hyperlink.type] = tokensFromTokenizables(hyperlinks)
  return tokenMap
}

let factory = HipChatTokenizationFactory()

extension DetectTokensAction {
  convenience init(message: String, onComplete: (result: [TokenType:Tokens])->()) {
    self.init(message: message, factory: factory, onComplete: onComplete)
  }
}

let expectationHandler: XCWaitCompletionHandler = { errorOrNil in
  if let error = errorOrNil {
    XCTFail("")
  }
}
