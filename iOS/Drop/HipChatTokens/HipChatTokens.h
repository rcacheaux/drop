//  Copyright (c) 2015 Atlassian. All rights reserved.

#import <UIKit/UIKit.h>

//! Project version number for HipChatTokens.
FOUNDATION_EXPORT double HipChatTokensVersionNumber;

//! Project version string for HipChatTokens.
FOUNDATION_EXPORT const unsigned char HipChatTokensVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HipChatTokens/PublicHeader.h>

#import "GTMDefines.h"
#import "GTMNSString+HTML.h"

@import Tokens;
