//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import Tokens

public class HipChatTokenizationFactory: TokenizationEngineFactory {
  public init() {}
  
  public func allTokenTypes() -> [TokenType] {
    return HipChatTokenType.allTypes()
  }
  
  public func detectorForType(type: TokenType) -> Detector? {
    return HipChatTokenType(rawValue: type)?.detector()
  }
  
  public func enricherForType(type: TokenType) -> Enricher? {
    return HipChatTokenType(rawValue: type)?.enricher()
  }
  
  public func stylistForType(type: TokenType) -> Stylelist? {
    return HipChatTokenType(rawValue: type)?.stylist()
  }
}

public enum HipChatTokenType: TokenType {
  case Mention = "HipChatMention"
  case Emoticon = "HipChatEmoticon"
  case Hyperlink = "HipChatHyperlink"
  
  static func allTypes() -> [TokenType] {
    return [HipChatTokenType.Mention.rawValue, HipChatTokenType.Emoticon.rawValue, HipChatTokenType.Hyperlink.rawValue]
  }
  
  public func detector() -> Detector? {
    switch self {
    case .Mention:
      return MentionTokenDetector(tokenType: self.rawValue)
    case .Emoticon:
      return EmoticonTokenDetector(tokenType: self.rawValue)
    case .Hyperlink:
      return HyperlinkTokenDetector(tokenType: self.rawValue)
    }
  }
  
  public func enricher() -> Enricher? {
    switch self {
    case .Mention, .Emoticon:
      return nil
    case .Hyperlink:
      return HyperlinkTokenEnricher()
    }
  }
  
  public func stylist() -> Stylelist? {
    switch self {
    case .Mention:
      return MentionTokenStylist()
    case .Emoticon:
      return EmoticonTokenStylist()
    case .Hyperlink:
      return HyperlinkTokenStylist()
    }
  }
}
