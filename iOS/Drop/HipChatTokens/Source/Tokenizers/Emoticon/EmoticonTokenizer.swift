//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import Tokens

public class EmoticonTokenDetector: RegexPatternTokenDetector {
  public init(tokenType: TokenType) {
    //  \\(                       match opening parenthesis
    //       (                    capture contents of emoticon for display
    //          [[:ascii:]-[()]]  character class (all ascii characters minus opening/closing parenthesis
    //          {1,15}            one to fifteen characters
    //       )                    close capture group
    //  \\)                       match closing parenthesis
    super.init(regexPattern: "\\(([[:ascii:]-[()]]{1,15})\\)", regexOptions: .CaseInsensitive, tokenType: tokenType, semanticDescriptionCaptureGroupIndex: 1)
  }
}

public class EmoticonTokenStylist: Stylelist {
  public init() {}
  
  public func stringAttributesForTokens(tokens: Tokens) -> TokensWithStringAttributes {
    var tokensWithStringAttributes = [Token:StringAttributes]()
    for (token, metadata) in tokens {
      tokensWithStringAttributes[token] =
        [NSForegroundColorAttributeName: UIColor.primaryColor(),
         NSFontAttributeName: UIFont.systemFontOfSize(20)]
    }
    return tokensWithStringAttributes
  }
}
