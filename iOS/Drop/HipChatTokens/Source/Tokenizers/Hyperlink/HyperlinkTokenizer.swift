//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import Tokens

public class HyperlinkTokenDetector: DataDetectorTokenDetector {
  public init(tokenType: TokenType) {
    super.init(checkingType: .Link, tokenType: tokenType)
  }
}

public class HyperlinkTokenEnricher: Enricher {
  private let titleExtractionOperationQueue = NSOperationQueue()
  
  public init() {}
  
  public func enrichTokens(var tokens: Tokens) -> Tokens {
    var titleExtractionActions: [Token: ExtractHTMLTitleFromURLAction] = [:]
    for (token, metadataDictionary) in tokens {
      if let action = ExtractHTMLTitleFromURLAction(urlString: token.string) {
        titleExtractionActions[token] = action
      }
    }
    titleExtractionOperationQueue.addOperations(titleExtractionActions.values.array, waitUntilFinished: true)
    for (token, action) in titleExtractionActions {
      tokens[token] = [tokenSemanticDescriptionKey: action.result]
    }
    return tokens
  }
}

public class HyperlinkTokenStylist: Stylelist {
  public init() {}
  
  public func stringAttributesForTokens(tokens: Tokens) -> TokensWithStringAttributes {
    var tokensWithStringAttributes = [Token:StringAttributes]()
    for (token, metadata) in tokens {
      tokensWithStringAttributes[token] =
        [NSForegroundColorAttributeName: UIColor.secondaryColor(),
         NSFontAttributeName: UIFont(name: "SourceCodePro-Regular", size: 19)!]
    }
    return tokensWithStringAttributes
  }
}

