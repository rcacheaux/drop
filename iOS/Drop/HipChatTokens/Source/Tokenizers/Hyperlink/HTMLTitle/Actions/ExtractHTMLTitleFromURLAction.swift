//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import Elements

public typealias StringResultClosure = (result: String)->()

public class ExtractHTMLTitleFromURLAction: AsyncAction {
  public private(set) var result = ""
  
  private let url: NSURL
  private let onComplete: StringResultClosure?
  private let networkCompletionQueue = NSOperationQueue()
  private let remoteAPI: RemoteAPI
  
  public init(url: NSURL, remoteAPI: RemoteAPI, onComplete: StringResultClosure?) {
    self.url = url
    self.onComplete = onComplete
    self.remoteAPI = remoteAPI
    super.init()
  }
  
  convenience init(url: NSURL, onComplete: StringResultClosure?) {
    self.init(url: url, remoteAPI: URLRemoteAPI(), onComplete: onComplete)
  }
  
  convenience public init?(var urlString: String, onComplete: StringResultClosure?) {
    if urlString.rangeOfString("http",
      options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil, locale: nil) == nil {
        urlString = "http://\(urlString)"
    }
    if let url = NSURL(string: urlString) {
      self.init(url: url, onComplete: onComplete)
    } else {
      self.init(url: NSURL(), onComplete: nil)
      return nil
    }
  }
  
  convenience public init?(urlString: String) {
    self.init(urlString: urlString, onComplete: nil)
  }
  
  override public func actionCompleted() {
    onComplete?(result: result)
  }
  
  override public func run() {
    remoteAPI.getHTMLFromURL(url, completionQueue: networkCompletionQueue) {
      [weak self] htmlStringOrNil, errorOrNil in
      if let strongSelf = self {
        if let error = errorOrNil {
          strongSelf.result = "[\(NSError.networkError().localizedDescription)]"
          strongSelf.finishExecutingOperation()
        } else if let html = htmlStringOrNil {
          let titleExtractionAction = ExtractTitleFromHTMLAction(html: html)
          titleExtractionAction.completionBlock = {
            [weak self, weak titleExtractionAction] in
            if let strongSelf = self, let strongAction = titleExtractionAction {
              strongSelf.result = strongAction.result
              strongSelf.finishExecutingOperation()
            }
          }
          strongSelf.networkCompletionQueue.addOperation(titleExtractionAction)
        } else {
          strongSelf.finishExecutingOperation()
        }
      }
    }
  }
}
