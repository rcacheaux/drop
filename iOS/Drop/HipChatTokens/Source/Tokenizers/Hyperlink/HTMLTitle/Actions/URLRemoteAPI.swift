//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation

public protocol RemoteAPI {
  func getHTMLFromURL(url: NSURL, completionQueue: NSOperationQueue, onComplete: (String?, NSError?)->())
}

class URLRemoteAPI: RemoteAPI {
  func getHTMLFromURL(url: NSURL, completionQueue: NSOperationQueue, onComplete: (String?, NSError?)->()) {
    let session = NSURLSession(configuration: NSURLSessionConfiguration.ephemeralSessionConfiguration(),
      delegate: nil, delegateQueue: completionQueue)
    let request = NSURLRequest(URL: url)
    let task = session.dataTaskWithRequest(request) {
      dataOrNil, responseOrNil, errorOrNil in
      if let error = errorOrNil {
        onComplete(nil, error)
        return
      } else if let data = dataOrNil {
        if let html = NSString(data: data, encoding: NSUTF8StringEncoding) as? String {
          onComplete(html, nil)
          return
        } else {
          onComplete(nil, NSError.networkResponseError())
          return
        }
      }
    }
    task.resume()
  }
}
