//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import Elements

public class ExtractTitleFromHTMLAction: Action {
  private let html: String
  public private(set) var result = ""
  
  public init(html: String) {
    self.html = html
    super.init()
  }
  
  override public func run() {
    if let regex = NSRegularExpression(
      // <title                             match opening title tag
      //         (?:                        non-capturing optional group for spaces and/or attributes
      //               +                    one to many spaces (space char visible in pattern string below)
      //                 (?:                non-capturing group
      //                     (?:'[^']*')    content inside single quotes, content can not have quote inside
      //                     |              or
      //                     (?:\"[^\"]*\") content inside double quotes
      //                     |              or
      //                     [^'\">]        any character not a single, double quote and not closing bracket
      //                  )*                zero to many non-capture group for attributes
      //         )?                         closing non-capturing optional group
      // >
      // (.*?)                              capture contents of title, DotMatchesLineSeparators mode turned on
      // </title\\s*>                       match ending title tag with optional spaces before closing bracket
      pattern: "<title(?: +(?:(?:'[^']*')|(?:\"[^\"]*\")|[^'\">])*)?>(.*?)</title\\s*>",
      options: .CaseInsensitive | .DotMatchesLineSeparators,
      error: nil)
    {
        result = titleMatchWithRegex(regex)
    }
  }
  
  func titleMatchWithRegex(regex: NSRegularExpression) -> String {
    if let result = regex.firstMatchInString(html, options: .allZeros, range: html.absoluteRange) {
      let title: NSString = (html as NSString).substringWithRange(result.rangeAtIndex(1))
      return title.gtm_stringByUnescapingFromHTML()
    }
    return ""
  }
  
}
