//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import Tokens

public class MentionTokenDetector: RegexPatternTokenDetector {
  public convenience init(tokenType: TokenType) {
    // @                  at sign
    //   (                capture mention name without at sign, used for display
    //     (?:            group, don't capture though
    //         \\w        word character (UTF8 setting turned on)
    //         |          or
    //         \\p{So}    symbol character (for emoji but not math symbols)
    //     )+             one to many times
    //   )                end capture group
    // (
    //    (?=https?)      look ahead, looks for mention next to link without space (s optional)
    //    |               or
    //    (?<!https?)     negative look behind, don't match word boundary if there's a link (s optional)
    //    \\b             word boundary only if there's no link after mention
    // )
    self.init(regexPattern: "@((?:\\w|\\p{So})+)((?=https?)|(?<!https?)\\b)", tokenType: tokenType, semanticDescriptionCaptureGroupIndex: 1)
  }
}

public class MentionTokenStylist: Stylelist {
  public init() {}
  
  public func stringAttributesForTokens(tokens: Tokens) -> TokensWithStringAttributes {
    var tokensWithStringAttributes = [Token:StringAttributes]()
    for (token, metadata) in tokens {
      tokensWithStringAttributes[token] =
        [NSForegroundColorAttributeName: UIColor.secondaryColor(),
         NSFontAttributeName: UIFont.boldSystemFontOfSize(20)]
    }
    return tokensWithStringAttributes
  }
}
