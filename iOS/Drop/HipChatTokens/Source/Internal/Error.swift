//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation

extension NSError {
  class func defaultError() -> NSError {
    return NSError(.Default, "Unknown error occured.")
  }
  
  class func networkError() -> NSError {
    return NSError(.Network, "Error accessing URL.")
  }
  
  class func networkResponseError() -> NSError {
    return NSError(.NetworkResponse, "Error reading URL response.")
  }
}

protocol Error {
  var code: Int { get }
  var domain: String { get }
}

enum HipChatTokensErrorCode: Int, Error {
  case Default
  case Network
  case NetworkResponse
  
  var domain: String {
    return "com.atlassian.hipchattokens.swift"
  }
  
  var code: Int {
    return self.rawValue
  }
}

extension NSError {
  convenience init(_ code: HipChatTokensErrorCode, _ localizedDescription: String) {
    self.init(domain: code.domain, code: code.code, userInfo: [NSLocalizedDescriptionKey: localizedDescription])
  }
}
