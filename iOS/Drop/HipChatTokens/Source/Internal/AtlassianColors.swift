//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation

extension UIColor {
  public class func primaryColor() -> UIColor {
    return UIColor(red: 89.0/255.0, green: 175.0/255.0, blue: 225.0/255.0, alpha: 1.0)
  }
  public class func secondaryColor() -> UIColor {
    return UIColor(red: 32.0/255.0, green: 80.0/255.0, blue: 129.0/255.0, alpha: 1.0)
  }
  public class func actionColor() -> UIColor {
    return UIColor(red: 103.0/255.0, green: 171.0/255.0, blue: 73.0/255.0, alpha: 1.0)
  }
}
