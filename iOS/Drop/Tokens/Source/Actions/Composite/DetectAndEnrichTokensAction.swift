//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import Elements

public class DetectAndEnrichTokensAction: AsyncAction {
  public private(set) var result = [TokenType:Tokens]()
  
  private let message: String
  private let factory: TokenizationEngineFactory
  private let onComplete: ( (result: [TokenType:Tokens])->() )?
  private let matchEnrichOperationQueue = NSOperationQueue()
  
  public init(message: String,
    factory: TokenizationEngineFactory, onComplete: ( (result: [TokenType:Tokens])->() )?) {
      self.message = message
      self.factory = factory
      self.onComplete = onComplete
      super.init()
  }
  
  override public func actionCompleted() {
    onComplete?(result: result)
  }
  
  override public func run() {
    let matchAction = DetectTokensAction(message: message, factory: factory) {
      result in
      let enrichAction = EnrichTokensAction(tokenMap: result, factory: self.factory) {
        result in
        self.result = result
        self.finishExecutingOperation()
      }
      self.matchEnrichOperationQueue.addOperation(enrichAction)
    }
    self.matchEnrichOperationQueue.addOperation(matchAction)
  }
}
