//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import Elements

public class DetectAndStyleTokensAction: Action {
  public private(set) var stringAttributeTokenMap = [TokenType:TokensWithStringAttributes]()
  
  private let message: String
  private let factory: TokenizationEngineFactory
  private let operationQueue = NSOperationQueue()
  
  public init(message: String, factory: TokenizationEngineFactory) {
    self.message = message
    self.factory = factory
    super.init()
  }
  
  public override func run() {
    let detectAction = DetectTokensAction(message: message, factory: factory)
    self.operationQueue.addOperations([detectAction], waitUntilFinished: true)
    
    let styleAction = StyleTokensAction(tokenMap: detectAction.result, factory: factory)
    styleAction.start()
    stringAttributeTokenMap = styleAction.stringAttributeTokenMap
  }
}
