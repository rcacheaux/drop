//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import Elements

public class DetectTokensAction: AsyncAction {
  public private(set) var result: [TokenType:Tokens] = [:]
  
  private let message: String
  private let factory: TokenizationEngineFactory
  private let onComplete: ((result: [TokenType:Tokens])->())?
  private let matchOperationQueue = NSOperationQueue()
  
  public init(message: String,
    factory: TokenizationEngineFactory, onComplete: ((result: [TokenType:Tokens])->())? ) {
      self.message = message
      self.factory = factory
      self.onComplete = onComplete
      super.init()
  }
  
  public convenience init(message: String, factory: TokenizationEngineFactory) {
    self.init(message: message, factory: factory, onComplete: nil)
  }
  
  override public func actionCompleted() {
    onComplete?(result: result)
  }
  
  override public func run() {
    var matchActions: [SingleTokenTypeDetectAction] = []
    for type in factory.allTokenTypes() {
      if let detector = factory.detectorForType(type) {
        let action = SingleTokenTypeDetectAction(message: message, tokenType: type, detector: detector)
        matchActions.append(action)
      }
    }
    let resultAction = DetectTokensResultAction(matchingActions: matchActions)
    resultAction.completionBlock = {
      [weak self, weak resultAction] in
      if let strongSelf = self, let strongAction = resultAction {
        strongSelf.result = strongAction.result
        strongSelf.finishExecutingOperation()
      }
    }
    matchOperationQueue.addOperations(matchActions + [resultAction], waitUntilFinished: false)
  }
  
}

class DetectTokensResultAction: Action {
  private let matchActions: [SingleTokenTypeDetectAction]
  private(set) var result: [TokenType:Tokens] = [:]
  
  init(matchingActions: [SingleTokenTypeDetectAction]) {
    self.matchActions = matchingActions
    super.init()
    for action in matchingActions {
      addDependency(action)
    }
  }
  
  override func run() {
    var tokenizationResult: [TokenType:Tokens] = [:]
    for action in matchActions {
      tokenizationResult[action.tokenType] = action.result
    }
    result = tokenizationResult
  }
}
