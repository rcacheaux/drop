//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import Elements

public class StyleTokensAction: Action {
  public private(set) var stringAttributeTokenMap = [TokenType:TokensWithStringAttributes]()
  
  private let tokenMap: [TokenType:Tokens]
  private let factory: TokenizationEngineFactory
  
  init(tokenMap: [TokenType:Tokens], factory: TokenizationEngineFactory) {
    self.tokenMap = tokenMap
    self.factory = factory
    super.init()
  }

  override public func run() {
    var attributeTokenMap = [TokenType:TokensWithStringAttributes]()
    for (type, tokens) in tokenMap {
      if let stylist = factory.stylistForType(type) {
        let tokensWithAttributes = stylist.stringAttributesForTokens(tokens)
        attributeTokenMap[type] = tokensWithAttributes
      }
    }
    self.stringAttributeTokenMap = attributeTokenMap
  }
  
}
