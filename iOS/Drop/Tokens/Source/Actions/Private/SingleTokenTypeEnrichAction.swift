//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import Elements

public class SingleTokenTypeEnrichAction: Action {
  public let tokenType: TokenType
  public private(set) var result = Tokens()
  
  private let tokens: Tokens
  private let enricher: Enricher
  
  public init(tokenType: TokenType, tokens: Tokens, enricher: Enricher) {
    self.tokenType = tokenType
    self.tokens = tokens
    self.enricher = enricher
    super.init()
  }
  
  override public func run() {
    result = enricher.enrichTokens(tokens)
  }
}
