//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import Elements

public class SingleTokenTypeDetectAction: Action {
  public let tokenType: TokenType
  public private(set) var result = Tokens()
  
  private let message: String
  private let detector: Detector
  
  public init(message: String, tokenType: TokenType, detector: Detector) {
    self.message = message
    self.tokenType = tokenType
    self.detector = detector
    super.init()
  }
  
  override public func run() {
    let tokens = detector.tokensInMessage(message)
    result = tokens
  }
  
}
