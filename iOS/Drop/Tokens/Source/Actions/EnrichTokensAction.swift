//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation
import Elements

public class EnrichTokensAction: AsyncAction {
  public private(set) var result: [TokenType:Tokens] = [:]
  
  private let tokenMap: [TokenType:Tokens]
  private let onComplete: (result: [TokenType:Tokens])->()
  private let factory: TokenizationEngineFactory
  private let enrichOperationQueue = NSOperationQueue()
  
  public init(tokenMap: [TokenType:Tokens],
    factory: TokenizationEngineFactory, onComplete: (result: [TokenType:Tokens])->()) {
      self.tokenMap = tokenMap
      self.factory = factory
      self.onComplete = onComplete
      super.init()
  }
  
  override public func actionCompleted() {
    onComplete(result: result)
  }
  
  override public func run() {
    var enrichActions: [SingleTokenTypeEnrichAction] = []
    for (type, tokens) in tokenMap {
      if let enricher = factory.enricherForType(type) {
        let action = SingleTokenTypeEnrichAction(tokenType: type, tokens: tokens, enricher: enricher)
        enrichActions.append(action)
      }
    }
    let resultAction = EnrichTokensResultAction(enrichActions: enrichActions, tokenMap: tokenMap)
    resultAction.completionBlock = {
      [weak self, weak resultAction] in
      if let strongSelf = self, let strongAction = resultAction {
        strongSelf.result = strongAction.result
        strongSelf.finishExecutingOperation()
      }
    }
    enrichOperationQueue.addOperations(enrichActions + [resultAction], waitUntilFinished: false)
  }
}

class EnrichTokensResultAction: Action {
  private let enrichActions: [SingleTokenTypeEnrichAction]
  private(set) var result: [TokenType: Tokens]
  
  init(enrichActions: [SingleTokenTypeEnrichAction], tokenMap: [TokenType: Tokens]) {
    self.enrichActions = enrichActions
    self.result = tokenMap
    super.init()
    for action in enrichActions {
      addDependency(action)
    }
  }
  
  override func run() {
    for action in enrichActions {
      result[action.tokenType] = action.result
    }
  }
}
