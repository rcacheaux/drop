//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation

public let tokenSemanticDescriptionKey = "semanticDescription"

public typealias TokenType = String

public typealias Tokens = [Token: TokenMetadata]

public typealias TokenMetadata = [String: AnyObject]

public typealias StringAttributes = [NSObject: AnyObject]

public typealias TokensWithStringAttributes = [Token:StringAttributes]


public struct Token: Hashable {
  public let type: TokenType
  public let string: String
  public let range: NSRange
  
  public init(type: TokenType, string: String, range: NSRange) {
    self.type = type
    self.string = string
    self.range = range
  }
  
  public var hashValue: Int {
    return type.hashValue ^ string.hashValue ^ range.hashValue
  }
}

public func ==(lhs: Token, rhs: Token) -> Bool {
  return lhs.type == rhs.type && lhs.string == rhs.string && lhs.range == rhs.range
}
