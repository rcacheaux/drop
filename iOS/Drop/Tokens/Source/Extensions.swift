//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation

public extension String {
  public var absoluteRange: NSRange {
    return NSRange(location: 0, length: count(utf16))
  }
}

public extension NSString {
  public var absoluteRange: NSRange {
    return NSRange(location: 0, length: length)
  }
}

extension NSRange {
  init(location: Int, length: Int) {
    self.location = location
    self.length = length
  }
}

extension NSRange: Hashable {
  public var hashValue: Int {
    return location.hashValue ^ length.hashValue
  }
}

public func ==(lhs: NSRange, rhs: NSRange) -> Bool {
  return lhs.location == rhs.location && lhs.length == rhs.length
}
