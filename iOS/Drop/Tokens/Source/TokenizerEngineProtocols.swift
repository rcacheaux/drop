//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation

// Implement these protocols to support additional token types.

public protocol TokenizationEngineFactory {
  func allTokenTypes() -> [TokenType]
  func detectorForType(type: TokenType) -> Detector?
  func enricherForType(type: TokenType) -> Enricher?
  func stylistForType(type: TokenType) -> Stylelist?
}

public protocol Detector {
  func tokensInMessage(message: String) -> Tokens
}

public protocol Enricher {
  func enrichTokens(tokens: Tokens) -> Tokens
}

public protocol Stylelist {
  func stringAttributesForTokens(tokens: Tokens) -> TokensWithStringAttributes
}
