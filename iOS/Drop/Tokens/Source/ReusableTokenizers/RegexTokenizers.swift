//  Copyright (c) 2015 Atlassian. All rights reserved.

import Foundation

public class RegexTokenDetector: Detector {
  private let tokenType: TokenType
  private let regex: NSRegularExpression
  
  init(tokenType: TokenType, regexFactory: ()->(NSRegularExpression?, NSError?)) {
    self.tokenType = tokenType
    switch regexFactory() {
    case (let regex, _) where regex != nil:
      self.regex = regex!
    case (_, let error) where error != nil:
      println(error!)
      fallthrough
    default:
      fatalError("Programmer Error: Could not compile NSRegularExpression pattern.")
    }
  }
  
  public func tokensInMessage(message: String) -> Tokens {
    var tokens = Tokens()
    if let matches = regex.matchesInString(message, options: nil, range: message.absoluteRange) as? [NSTextCheckingResult] {
      for result in matches {
        let token = tokenWithResult(result, fromMessage: message)
        let metadata = metadataForToken(token, result: result, fromMessage: message)
        tokens[token] = metadata
      }
    }
    return tokens
  }
  
  public func tokenWithResult(result: NSTextCheckingResult, fromMessage message: String) -> Token {
    return Token(type: self.tokenType, string: (message as NSString).substringWithRange(result.range), range: result.range)
  }
  
  public func metadataForToken(token: Token, result: NSTextCheckingResult, fromMessage message: String) -> TokenMetadata {
    return [:]
  }
}


public class RegexPatternTokenDetector: RegexTokenDetector {
  private let semanticDescriptionCaptureGroupIndexOrNil: Int?
  
  public init(regexPattern: String, regexOptions: NSRegularExpressionOptions, tokenType: TokenType, semanticDescriptionCaptureGroupIndex: Int?) {
    self.semanticDescriptionCaptureGroupIndexOrNil = semanticDescriptionCaptureGroupIndex
    super.init(tokenType: tokenType, regexFactory: {
      var errorOrNil: NSError?
      return (NSRegularExpression(pattern: regexPattern, options:regexOptions, error: &errorOrNil), errorOrNil)
    })
  }
  
  public convenience init(regexPattern: String, tokenType: TokenType, semanticDescriptionCaptureGroupIndex: Int?) {
    let defaultRegexOptions: NSRegularExpressionOptions =
      (.UseUnicodeWordBoundaries | .CaseInsensitive | .DotMatchesLineSeparators)
    self.init(regexPattern: regexPattern, regexOptions: defaultRegexOptions, tokenType: tokenType, semanticDescriptionCaptureGroupIndex: semanticDescriptionCaptureGroupIndex)
  }
  
  override public func metadataForToken(token: Token, result: NSTextCheckingResult, fromMessage message: String) -> TokenMetadata {
    if let semanticDescriptionCaptureGroupIndex = semanticDescriptionCaptureGroupIndexOrNil {
      if result.numberOfRanges > semanticDescriptionCaptureGroupIndex {
        let range = result.rangeAtIndex(semanticDescriptionCaptureGroupIndex)
        let semanticDescription = (message as NSString).substringWithRange(range)
        return [tokenSemanticDescriptionKey: semanticDescription]
      }
    }
    return [:]
  }
}

public class DataDetectorTokenDetector: RegexTokenDetector {
  public init(checkingType: NSTextCheckingType, tokenType: TokenType) {
    super.init(tokenType: tokenType, regexFactory: {
      var errorOrNil: NSError?
      return (NSDataDetector(types: checkingType.rawValue, error: &errorOrNil), errorOrNil)
    })
  }
}
