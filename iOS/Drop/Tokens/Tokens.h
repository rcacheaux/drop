//  Copyright (c) 2015 Atlassian. All rights reserved.

#import <UIKit/UIKit.h>

//! Project version number for Tokens.
FOUNDATION_EXPORT double TokensVersionNumber;

//! Project version string for Tokens.
FOUNDATION_EXPORT const unsigned char TokensVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Tokens/PublicHeader.h>

@import Elements;
